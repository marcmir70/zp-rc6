***Keywords***
## Login
Acesso a página Login
    Go To       ${base_url}

Submeto minhas credenciais
    [Arguments]     ${email}    ${password}

    Login With      ${email}    ${password}

Devo ver a área logada
    # Wait Until Element Is Visible      //strong[text()='Sair']     5
    Wait Until Page Contains    Aluguéis    5

Devo ver toaster com mensagem
    [Arguments]     ${expected_message}

    Wait Until Element Contains     ${TOASTER_ERROR_P}    ${expected_message}

## Save Customers ##################################################
Dado que acesso o formulário de cadastro de clientes
    Go To Customers
    Wait Until Element is Visible       ${CUSTOMERS_FORM}     5
    Click Element                       ${CUSTOMERS_FORM}

E que eu tenho o seguinte cliente
    [Arguments]     ${name}     ${cpf}      ${address}      ${phone_number}
# de volta para o passado!
    Remove Customer By Cpf      ${cpf}

    Set Test Variable   ${name}
    Set Test Variable   ${cpf}
    Set Test Variable   ${address}
    Set Test Variable   ${phone_number}

Mas esse CPF já existe no sistema
    Insert Customer     ${name}     ${cpf}      ${address}      ${phone_number}

Quando faço a inclusão desse cliente
    Register New Customer     ${name}     ${cpf}      ${address}      ${phone_number}

# Usada em Customers e Equipos
Então devo ver a notificação:
    [Arguments]     ${expect_notice} 

    Wait Until Element Contains     ${TOASTER_SUCCESS}    ${expect_notice}    5

Então devo ver a notificação de erro:
    [Arguments]     ${expect_notice} 

    Wait Until Element Contains     ${TOASTER_ERROR}    ${expect_notice}    5

Então devo ver mensagens informando que os campos do cadastro de clientes são Obrigatórios
    Wait Until Element Contains     ${LABEL_NAME}        Nome é obrigatório          5
    Wait Until Element Contains     ${LABEL_CPF}         CPF é obrigatório           5
    Wait Until Element Contains     ${LABEL_ADDRESS}     Endereço é obrigatório      5
    Wait Until Element Contains     ${LABEL_PHONE}       Telefone é obrigatório      5

# 1º Desafio do Robo Camp 6ª edição
# serve para validar cadastro de Customers ou Equipos
Então devo ver erro no campo
    [Arguments]     ${error_message}
    
    Wait Until Page Contains    ${error_message}    3

E esse cliente deve ser exibido na lista
    ${cpf_formatado}=                   Format Cpf      ${cpf}
    Go Back
    Wait Until Element is Visible       ${CUSTOMER_LIST}       5
    Table Should Contain                ${CUSTOMER_LIST}       ${cpf_formatado}

## Remove Customer ##################################################

Dado que eu tenho um cliente indesejado
    [Arguments]         ${name}      ${cpf}       ${address}       ${phone_number}
    Remove Customer By cpf           ${cpf}
#    Register New Customer  name  cpf  address  phone_number
    Insert Customer     ${name}      ${cpf}       ${address}       ${phone_number}

    Set Test Variable   ${cpf}
    
E acesso a lista de clientes
    Go To Customers

Quando eu removo este cliente
    # Format Cpf é a keyword que representa o método no arquivo db.py
    ${cpf_formatado}=    Format Cpf      ${cpf}
    Set Test Variable       ${cpf_formatado}

# Page Objects
    Go To Customer Details      ${cpf_formatado}
    Click Remove Customer

E esse cliente não deve aparecer na lista
    Wait Until Page Does Not Contain        ${cpf_formatado}

# 2º Desafio do RoboCamp 6
## Equipos ##################################################
Dado que acesso o formulário de cadastro de equipamentos
    Wait Until Element is Visible       ${NAV_EQUIPOS}      5
    Click Element                       ${NAV_EQUIPOS}
    Wait Until Element is Visible       ${EQUIPOS_FORM}     5
    Click Element                       ${EQUIPOS_FORM}

E que eu tenho o seguinte equipamento e diária
    [Arguments]     ${eq_name}     ${eq_price}
# keyword para o método (função) Python em libs
    Remove Equipo By Name      ${eq_name}
# variáveis disponíveis no escopo do teste
    Set Test Variable   ${eq_name}
    Set Test Variable   ${eq_price}

Mas esse equipamento já existe no sistema
    Insert Equipo       ${eq_name}     ${eq_price}

Quando faço a inclusão desse equipamento
    Register New Equipo     ${eq_name}     ${eq_price}
    Sleep   5

# ({ o keyword abaixo é o mesmo para Customers })
# Então devo ver a notificação:
#     [Arguments]     ${expect_notice} 
#     Wait Until Element Contains     ${TOASTER_SUCCESS}    ${expect_notice}    5
#     Sleep   5

Então devo ver mensagens informando que os campos do cadastro de equipamentos são obrigatórios
    Wait Until Element Contains     ${LABEL_EQ_NAME}        Nome do equipo é obrigatório        5
    Wait Until Element Contains     ${LABEL_EQ_PRICE}       Diária do equipo é obrigatória      5
