***Settings***
Documentation       Representação da página Clientes com seus elementos e ações

***Variables***
${CUSTOMERS_FORM}   css:a[href$=register]
${LABEL_NAME}       css:label[for=name]
${LABEL_CPF}        css:label[for=cpf]
${LABEL_ADDRESS}    css:label[for=address]
${LABEL_PHONE}      css:label[for=phone_number]
${CUSTOMER_LIST}    css:table

***Keywords***
Register New Customer
    [Arguments]     ${name}     ${cpf}      ${address}      ${phone_number}

    Input Text      id:name             ${name}
    Input Text      id:cpf              ${cpf}
    Input Text      id:address          ${address}
    Input Text      id:phone_number     ${phone_number}
    Sleep   5

    Click Element   xpath://button[text()='CADASTRAR']      
    Sleep   5
    # encontrei o locator acima também como //*[@id="root"]/div[1]/div[2]/div/header/section/button[2]

Go To Customer Details
    [Arguments]     ${cpf_formatado}
    ${element}=     Set Variable        xpath://td[text()='${cpf_formatado}']
    Wait Until Element is Visible       ${element}     5
    Click Element                       ${element}
    
Click Remove Customer
    ${element}=     Set Variable        xpath://button[text()='APAGAR']
    Wait Until Element is Visible       ${element}         5
    Click Element                       ${element}    
