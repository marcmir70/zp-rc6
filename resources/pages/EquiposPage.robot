# 2º Desafio do RoboCamp 6
***Settings***
Documentation       Representação da página Equipamentos com seus elementos e ações

***Variables***
# pegadinha... ;)
${EQUIPOS_FORM}         css:a[href$='/equipos/register']
${LABEL_EQ_NAME}        css:label[for=equipo-name]
${LABEL_EQ_PRICE}       css:label[for=daily_price]

***Keywords***
Register New Equipo
    [Arguments]     ${eq_name}     ${eq_price}

    Input Text      id:equipo-name      ${eq_name}
    Input Text      id:daily_price      ${eq_price}
    Sleep   3

    Click Element   xpath://button[text()='CADASTRAR']
    Sleep   3