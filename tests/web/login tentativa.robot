***Settings***
Documentation       Login Tentativa

Resource    ../../resources/base.robot

# Gancho de Suíte - executa keywords só uma vez antes da execução de todos os casos de teste
Suite Setup          Start Session
# Gancho de Suíte - executa keywords só uma vez antes da execução de todos os casos de teste
Suite Teardown       Finish Session

Test Template       Tentativa de Login

***Keywords***
Tentativa de Login
    [Arguments]     ${input_email}      ${input_senha}      ${output_message}

    Acesso a página Login
    Submeto minhas credenciais          ${input_email}      ${input_senha}
    Devo ver toaster com mensagem       ${output_message}

***Test Cases***
Senha Incorreta             admin@zepalheta.com.br      pww112      Ocorreu um erro ao fazer login, cheque as credenciais.
Senha em branco             admin@zepalheta.com.br      ${EMPTY}    O campo senha é obrigatório
Email em branco             ${EMPTY}                    pww112      O campo email é obrigatório!
Email e senha em branco     ${EMPTY}                    ${EMPTY}    Os campos email e senha não foram preenchidos!
Login incorreto             admin&gmail.com             abc123      Ocorreu um erro ao fazer login, cheque as credenciais.
