***Settings***
Documentation       Cadastro de clientes

Resource    ../../resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session    

# 2º Desafio do RoboCamp 6
***Test Cases***
Novo Equipamento
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento e diária
    ...     Bateria Eletrônica   133,49
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação:   Equipo cadastrado com sucesso!

Equipamento Duplicado
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento e diária
    ...     Harpa   101,01
    Mas esse equipamento já existe no sistema
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação de erro:   Erro na criação de um equipo
# Ocorreu um error na criação de um equipo, tente novamente mais tarde!
# experaria (como no cadastro_clientes) >> Este equipamento já existe no sistema

# aproveitando solução do 1º Desafio...
Nome obrigatório
    [Template]  Validação de Campos
    ${EMPTY}    139,99      Nome do equipo é obrigatório

Valor obrigatório
    [Template]  Validação de Campos
    Saxofone    ${EMPTY}    Diária do equipo é obrigatória
# Bug encontrado: se inserir 0 não reclama!

***Keywords***
Validação de Campos
    [Arguments]     ${eq_name}      ${eq_price}     ${saida}
    
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento e diária    ${eq_name}    ${eq_price}
    Quando faço a inclusão desse equipamento
    Então devo ver erro no campo    ${saida}

#desafio
# clientes    já testado
# equipos     2 campos
# alugueis    

# implementar equipamentos.robot
# Data de Entrega : terça, 8 de Setembro de 2020 até 10h da manhã
# Entrar nos comentários da video aula e no email fernando@qaninja.com.br // cc oi@qaninja.com.br

# Prêmio : 5 primeiros que entregarem ganharão uma vaga no DevTester!
  # * Inscrições que se encerrarão amanhã!