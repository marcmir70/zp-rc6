***Settings***
Documentation       Cadastro Tentativa

Resource    ../../resources/base.robot

Suite Setup         Login Session
Suite Teardown      Finish Session

Test Template       Tentativa de Cadastro

# 1º Desafio do Robo Camp 6ª edição

# Implementar os cenários de campos obrigatórios usando a técnica do Test Template
# Premio: os 10 primeiros que entregarem até o dia 1º de Setembro de 2020 até 2020, 19hs
# ganharão uma vaga no curso Performance Tests - https://cursos.qaninja.com.br/performance

# Entrega: subir seu código no GitLab e enviar o link nos comentários da video aula!
***Keywords***
Tentativa de Cadastro
    [Arguments]     ${name}     ${cpf}      ${address}      ${phone_number}     ${output_message}
    
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...             ${name}     ${cpf}      ${address}      ${phone_number}
    Então devo ver erro no campo            ${output_message}

# Cenarios
# Nome é obrigatório
# CPF é obrigatório
# Endereço é obrigatório
# Telefone é obrigatório

***Test Cases***
Nome obrigatório        ${EMPTY}        12345678900     Rua Bug, 1      21987654321     Nome é obrigatório
CPF obrigatório         Zen Defeito     ${EMPTY}        Avenida Null    11991860000     CPF é obrigatório
Endereço obrigatório    Caio Bug        12345678900     ${EMPTY}        51910910910     Endereço é obrigatório
Telefone obrigatório    Crash Ops       12345678900     Lgo. Err, -1    ${EMPTY}        Telefone é obrigatório
