***Settings***
Documentation       Cadastro Tentativa

Resource    ../resources/base.robot

Suite Setup          Start Session
Suite Teardown       Finish Session

Test Template       Tentativa de Cadastro

***Keywords***
Tentativa de Login
    [Arguments]     ${input_nome}      ${input_cpf}      ${input_ender}      ${input_tel}    ${output_message}

    Acesso a página Login
    Submeto minhas credenciais          ${input_email}      ${input_senha}
    Devo ver toaster com mensagem       ${output_message}

***Test Cases***
Nome obrigatório        ${EMPTY}        12345678900     Rua dos Bugs, 0     21987654321     Nome é obrigatório
CPF obrigatório         Zen Defeito     ${EMPTY}        Avenida Null        11991860000     CPF é obrigatório
Endereço obrigatório    Caiu Vasco      12345678900     ${EMPTY}            51910910910     Endereço é obrigatório
Telefone obrigatório    Crash Ops       12345678900     Largo Buraco, -1    ${EMPTY}        Telefone é obrigatório

Nome e CPF obrigatórios                     ${EMPTY}        12345678900     Rua dos Bugs, 0     21987654321     Nome é obrigatório          CPF é obrigatório       ${EMPTY}                    ${EMPTY}
Nome e Endereço obrigatórios                Zen Defeito     ${EMPTY}        Avenida Null        11991860000     Nome é obrigatório          ${EMPTY}                Endereço é obrigatório      ${EMPTY}
Nome e Telefone obrigatórios                Caiu Vasco      12345678900     ${EMPTY}            51910910910     Nome é obrigatório          ${EMPTY}                ${EMPTY}                    Telefone é obrigatório
CPF e Telefone obrigatórios                 Crash Ops       ${EMPTY}        Largo Buraco, -1    ${EMPTY}        ${EMPTY}                    CPF é obrigatório       ${EMPTY}                    Telefone é obrigatório
CPF e Endereço obrigatórios                 Crash Ops       ${EMPTY}        ${EMPTY}            ${EMPTY}        ${EMPTY}                    CPF é obrigatório       Endereço é obrigatório      ${EMPTY}
Endereço e Telefone obrigatórios            Crash Ops       12345678900     ${EMPTY}            ${EMPTY}        ${EMPTY}                    ${EMPTY}                Endereço é obrigatório      Telefone é obrigatório
Nome, CPF e Endereço obrigatórios           ${EMPTY}        ${EMPTY}        ${EMPTY}            51910910910     Nome é obrigatório          CPF é obrigatório       Endereço é obrigatório      ${EMPTY}
Nome, CPF e Telefone obrigatórios           ${EMPTY}        ${EMPTY}        Largo Buraco, -1    ${EMPTY}        Nome é obrigatório          CPF é obrigatório       ${EMPTY}                    Telefone é obrigatório
Nome, Endereço e Telefone obrigatórios      ${EMPTY}        12345678900     ${EMPTY}            ${EMPTY}        Nome é obrigatório          ${EMPTY}                Endereço é obrigatório      Telefone é obrigatório
CPF, Endereço e Telefone obrigatórios       Crash Ops       ${EMPTY}        ${EMPTY}            ${EMPTY}        ${EMPTY}                    CPF é obrigatório       Endereço é obrigatório      Telefone é obrigatório
Todos campos obrigatórios                   ${EMPTY}        ${EMPTY}        ${EMPTY}            ${EMPTY}        Nome é obrigatório          CPF é obrigatório       Endereço é obrigatório      Telefone é obrigatório