***Settings***
Documentation       Cadastro de clientes

Resource    ../../resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session    

***Test Cases***
Novo cliente
    [Tags]      smoke
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente
    ...     Bon Jovi    00000001406     Rua dos Bugs, 1000      11999888777
    Quando faço a inclusão desse cliente
    Então devo ver a notificação:   Cliente cadastrado com sucesso!
    E esse cliente deve ser exibido na lista

Cliente Duplicado
    [Tags]  Duplicado
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente
    ...     Adrian Smith    00000014141     Rua dos Bugs, 2000      11999888776
    Mas esse CPF já existe no sistema
    Quando faço a inclusão desse cliente
    Então devo ver a notificação de erro:   Este CPF já existe no sistema!

Campos Obrigatórios
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente
    ...     ${EMPTY}    ${EMPTY}    ${EMPTY}    ${EMPTY}
    Quando faço a inclusão desse cliente
    Então devo ver mensagens informando que os campos do cadastro de clientes são Obrigatórios

# solução proposta para o 1º Desafio RoboCamp 6ª edição...
Nome é obrigatório
    [Tags]      required
    [Template]  Validação de Campos
    ${EMPTY}            01245678912     Rua dos QAs, 1      11991860000     Nome é obrigatório

CPF é obrigatório
    [Tags]      required
    [Template]  Validação de Campos
    Marcelo Miranda     ${EMPTY}         Rua dos QAs, 1     11991860000     CPF é obrigatório

Endereço é obrigatório
    [Tags]      required
    [Template]  Validação de Campos
    Marcelo Miranda     01245678912     ${EMPTY}            11991860000     Endereço é obrigatório

Telefone é obrigatório
    [Tags]      required
    [Template]  Validação de Campos
    Marcelo Miranda     01245678912     Rua dos QAs, 1      ${EMPTY}        Telefone é obrigatório

Telefone incorreto
    [Template]  Validação de Campos
    João da Silva       0000001406     Rua dos Bugs, 100    1199999999      Telefone inválido

***Keywords***
Validação de Campos
    [Arguments]     ${nome}     ${cpf}      ${endereco}      ${telefone}     ${saida}
    
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente
    ...             ${nome}     ${cpf}      ${endereco}      ${telefone}
    Quando faço a inclusão desse cliente
    Então devo ver erro no campo            ${saida}