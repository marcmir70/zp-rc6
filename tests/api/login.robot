*** Settings ***
Resource        ../../resources/services.robot

*** Test Cases ***
Get Requests
    [tags]      get_started
### código abaixo copiado de https://github.com/MarketSquare/robotframework-requests#readme
    Create Session    github         http://api.github.com
#    Create Session    google         http://www.google.com
#    ${resp}=          Get Request    google               /
#    Status Should Be  200            ${resp}
    ${resp}=          Get Request    github               /users/bulkan
### não é interessante por não apresentar o código de retorno (se erro, qual erro, por exemplo)
    Request Should Be Successful     ${resp}
    Dictionary Should Contain Value  ${resp.json()}       Bulkan Evcimen

Login com Sucesso
    [Tags]      mod4_class2
    ${resp}=        Post Session        admin@zepalheta.com.br      pwd123
#    Request Should Be Successful     ${resp}
    Status Should Be    200     ${resp}

Senha Incorreta
    [Tags]      mod4_class2
    ${resp}=        Post Session        admin@zepalheta.com.br      abs123
    Status Should Be    401     ${resp}

Usuário não existe
    [Tags]      mod4_class2
    ${resp}=        Post Session        404@zepalheta.com.br        abc123
    Status Should Be    401     ${resp}

Empty email
    [Tags]      mod4_class2_complementary
    ${resp}=        Post Session        ${EMPTY}      abs123
    Status Should Be    400     ${resp}

Empty Password
    [Tags]      mod4_class2_complementary
    ${resp}=        Post Session        admin@zepalheta.com.br      ${EMPTY}
    Status Should Be    400     ${resp}

Campo que não deve ser enviado
    [Tags]      mod4_class2_added
    Create Session    zp-api         http://zepalheta-api:3333

    &{headers}=     Create Dictionary       Content-Type=application/json
    &{payload}=     Create Dictionary       email=404@zepalheta.com.br    password=abc123       name=Fulano de Tal

    ${resp}=          Post Request      zp-api      /sessions       data=${payload}     headers=${headers}
#    Request Should Be Successful     ${resp}
    Status Should Be    400     ${resp}